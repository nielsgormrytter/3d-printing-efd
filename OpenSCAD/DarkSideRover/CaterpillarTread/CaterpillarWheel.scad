include <TreadDimensions.scad>
$fn=60;
faceSize = totalElementLength - 1.8 * cylinderRadius; //constant found by trial and error
faceAngle = 360/nbFaces;
wheelRadius = faceSize/2 / sin(faceAngle/2);
faceRadius = wheelRadius * cos(faceAngle/2);
difference() {
    linear_extrude(wheelDepth) {
        polygon([for (i=[0:nbFaces-1], a=i*faceAngle) [ wheelRadius*cos(a), wheelRadius*sin(a) ]]);
    }
    for(i=[0:nbFaces-1], a=i*faceAngle) {
        translate([(wheelRadius-(2 * cylinderRadius - plateThikness))*cos(a), (wheelRadius - (2 * cylinderRadius - plateThikness))*sin(a), -0.1]) cylinder(wheelDepth/2 - 1.0, r=cylinderRadius+0.1);
        translate([(wheelRadius-(2 * cylinderRadius - plateThikness))*cos(a), (wheelRadius - (2 * cylinderRadius - plateThikness))*sin(a), wheelDepth/2 + 1.0]) cylinder(wheelDepth/2+0.2, r=cylinderRadius+0.1);

        translate([wheelRadius*cos(a), wheelRadius *sin(a), -0.1]) cylinder(wheelDepth/2 - 1.0, r=cylinderRadius+1);
        translate([wheelRadius*cos(a), wheelRadius *sin(a), wheelDepth/2 + 1.0]) cylinder(wheelDepth/2+0.2, r=cylinderRadius+1);
    }
    for(i=[0:nbFaces-1], a=(i+0.5)*faceAngle) {
        translate([(faceRadius+0.1)*cos(a), (faceRadius+0.1)*sin(a), wheelDepth/2 - (3+0.8)/2]) rotate([0,0,180+a]) linear_extrude(3 + 0.8) {
            polygon([[0,-2.5-0.3], [8 + 0.4, 2.0-2.5], [8 + 0.4, 3.0-2.5], [0,5-2.5+0.3]]);
        }
    }
    translate([0,0,-0.1]) linear_extrude(wheelDepth+0.2) {
        polygon([for (i=[0:6-1], a=i*360/6) [ 5*cos(a), 5*sin(a) ]]);
    }
}
