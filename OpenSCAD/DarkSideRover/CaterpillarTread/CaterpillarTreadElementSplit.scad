$fn=60;
splitHeight = 12;
difference() {
    union(){
        cylinder(splitHeight, r=1.4);
        cylinder(1, r=1.6);
        translate([0,0, splitHeight-1]) cylinder(1, r=1.6);
    }
    translate([-3,-0.55,splitHeight-4]) cube([6,1.1,55]);
}
