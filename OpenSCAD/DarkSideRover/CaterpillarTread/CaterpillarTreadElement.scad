include <TreadDimensions.scad>
$fn=60;
difference() {
    union() {
        linear_extrude(plateThikness) {
            polygon([[0,0], [25,0], [30,totalElementLength - 2 * cylinderRadius], [-5, totalElementLength - 2 * cylinderRadius]]);
        }
        translate([14, totalElementLength/2 - 2.5 - cylinderRadius, 1.5]) rotate([0, -90, 0]) linear_extrude(3) {
            polygon([[0,0], [8, 2.0], [8, 3.0], [0,5]]);
        }
        translate([5, 0, cylinderRadius]) rotate([0, 90, 0]) cylinder(15, r=cylinderRadius);
        translate([0, totalElementLength + -2 * cylinderRadius, cylinderRadius]) rotate([0, 90, 0]) cylinder(25, r=cylinderRadius);
    }
    translate([-1, 0, cylinderRadius]) rotate([0, 90, 0]) cylinder(40, r=1.5); 
    translate([-1, totalElementLength - 2 * cylinderRadius, cylinderRadius]) rotate([0, 90, 0]) cylinder(40, r=1.5);
    translate([-0.7, -cylinderRadius, -0.1]) cube([5+0.7, cylinderRadius*2 + 0.1 ,3]);
    translate([10, -cylinderRadius, -0.1]) cube([5+0.0, cylinderRadius*2 + 0.1 ,10]);
    translate([20, -cylinderRadius, -0.1]) cube([5+0.7, cylinderRadius*2 + 0.1 ,3]);

    translate([5-0.1, totalElementLength - 3*cylinderRadius, -1]) cube([15+0.2, 10 ,10]);
    translate([25, totalElementLength - 3*cylinderRadius, -1]) cube([15, 10 ,10]);
    translate([-15, totalElementLength - 3*cylinderRadius, -1]) cube([15, 10 ,10]);
}
