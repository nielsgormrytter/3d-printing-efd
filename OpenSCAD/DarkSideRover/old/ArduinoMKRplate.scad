$fn=60;
merkurHoleRadius = 2.0;
thickness=1.75;
difference() {
    translate([0,0,0]) cube([60,40,thickness]);
        for (i=[5:10:55]) {
            for (j=[5:10:35]) {
                translate([i, j, 0]) cylinder(h=thickness, r=merkurHoleRadius);
            }
        }
        for (i=[1:14]) {
                translate([10+i*2.54, 10, 0]) cylinder(h=thickness, r=0.6);
                translate([10+i*2.54, 30, 0]) cylinder(h=thickness, r=0.6);
        }
}