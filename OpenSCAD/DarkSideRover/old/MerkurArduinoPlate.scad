//Arduino hole spacing from
//https://www.flickr.com/photos/johngineer/5484250200/sizes/o/in/photostream/
$fn=60;
thickness=1.75;
arduinoHoleRadius = 1.6;
merkurHoleRadius = 2.0;
difference() {
    translate([10,-2,0]) cube([60,60,thickness]);
    union() {
        translate([14+1.3, 2.5, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        translate([14+1.3, 53.3 - 2.5, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        translate([14+1.3+50.8, 7.3, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        translate([14+1.3+50.8, 7.3+27.9, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        for (i=[20:10:60]) {
            for (j=[8:10:50]) {
                translate([i, j, 0]) cylinder(h=thickness, r=merkurHoleRadius);
            }
        }
    }
}