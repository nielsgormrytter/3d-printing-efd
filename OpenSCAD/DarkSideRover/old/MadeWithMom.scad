$fn=60;
thickness=1.75;
plateLength = 100;
arduinoHoleRadius = 1.6;
merkurHoleRadius = 2.0;
difference() {
    union(){
        cube([plateLength,18,thickness]);
        linear_extrude(height = 3){
            translate([13.5,5,1]) scale([0.8, 0.8, 1]) text("MadeWithMom");
        }
    }
    translate([5, 9, 0]) cylinder(h=thickness, r=merkurHoleRadius);
    translate([plateLength-5, 9, 0]) cylinder(h=thickness, r=merkurHoleRadius);
}
