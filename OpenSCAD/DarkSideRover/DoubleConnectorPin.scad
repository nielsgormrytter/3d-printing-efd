
module DoubleConnectorPin(length, diameter) {
    rotate([90,0,0])
    difference() {
        translate([0,diameter/2-diameter/8,0]) difference() {
            union() {
                cylinder(h=length,d=diameter);
                cylinder(h=1,d=diameter + 0.3);
                translate([0, 0, length-1]) cylinder(h=1,d=diameter + 0.3);
                translate([0, 0, length-4]) cylinder(h=1,d=diameter + 0.3);
                translate([0, 0, 3]) cylinder(h=1,d=diameter + 0.3);
            }
            cube([diameter/3,5,6], center=true);
            translate([0,0,length+1]) cube([diameter/3,5,8], center=true);
        }
        rotate([90,0,0]) translate([-diameter/2, 0,0]) cube([diameter, length, 3]);
    }
}