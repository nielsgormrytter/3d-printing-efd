arduinoHoleRadius = 1.5;
thickness=1.75;
difference(){
    cube( [101,116,thickness]);
    translate([5,10,0])cylinder( d=4, h=thickness,$fn=100);
    translate([96,10,0])cylinder( d=4, h=thickness,$fn=100);
    translate([5,105,0])cylinder( d=4, h=thickness,$fn=100);
     translate([96,105,0])cylinder( d=4, h=thickness,$fn=100);
     translate([8+1.3, 17+2.5, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
     translate([8+1.3, 17+53.3 - 2.5, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
     translate([8+1.3+50.8, 17+7.3, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
     translate([8+1.3+50.8, 17+7.3+27.9, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
}
difference() {
    translate([10,0,-11]) cube([24,18,11]);
       translate([11,-1,-10]) cube([22,15,11]);
}

difference() {
    translate([70,0,-11]) cube([24,18,11]);
       translate([71,-1,-10]) cube([22,15,11]);
}
difference() {
    translate([70,96,-11]) cube([24,18,11]);
       translate([71,100,-10]) cube([22,15,11]);
}
difference() {
    translate([10,96,-11]) cube([24,18,11]);
       translate([11,100,-10]) cube([22,15,11]);
}