$fn = 100;
batteryDiameter = 18;
holderHeight = 15;
difference() {
    union(){
        cylinder(h=holderHeight, d=batteryDiameter+2.5);
        translate([batteryDiameter+ 0.6 + 0.2, 0, 0]) cylinder(h=holderHeight, d=batteryDiameter+2.5);
    }
    translate([0, 0, 1.5]) cylinder(h=holderHeight, d=batteryDiameter+0.5);
    translate([batteryDiameter + 0.6 + 0.2, 0, 1.5]) cylinder(h=holderHeight, d=batteryDiameter+0.5);
    translate([0, 0, 0]) cylinder(h=holderHeight, d=3);
    translate([batteryDiameter + 0.6 + 0.2, 0, 0]) cylinder(h=holderHeight, d=3);
}    
    