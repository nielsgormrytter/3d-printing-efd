$fn=60;
module droneWing() {
    difference() {
        union() {
            cube([100,8,1]);
            translate([20,3,0]) cube([60,1,4]);
        }
        translate([4,4,0]) cylinder(d=2,1);
        translate([4+12,4,0]) cylinder(d=2,1);
        translate([4+6,4,0]) cylinder(d=4,1);
        translate([100-4,4,0]) cylinder(d=2,1);
        translate([100-4-12,4,0]) cylinder(d=2,1);
        translate([100-4-6,4,0]) cylinder(d=4,1);
    }
}

droneWing();
translate([54,-46,0]) rotate([0,0,90]) droneWing();