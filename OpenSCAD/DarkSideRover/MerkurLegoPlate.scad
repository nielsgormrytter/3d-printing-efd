//Arduino hole spacing from
//https://www.flickr.com/photos/johngineer/5484250200/sizes/o/in/photostream/
$fn=60;
thickness=1.75;
arduinoHoleRadius = 1.5;
merkurHoleRadius = 2.0;

length=4;
width=2;
cylinder_size=4.9;
difference() {
    translate([0,0,0]) cube([length*10,width*10,thickness]);
         translate([5, 5, 0]) cylinder(h=thickness, r=merkurHoleRadius);
        translate([5, width*10-5, 0]) cylinder(h=thickness, r=merkurHoleRadius);
        translate([length*10-5, 5, 0]) cylinder(h=thickness, r=merkurHoleRadius);
        translate([length*10-5, width*10-5, 0]) cylinder(h=thickness, r=merkurHoleRadius);
        }
for(j = [12:8:30]){
    for(i = [6:8:20]){
        translate([j,i,0])cylinder(d=cylinder_size,h=3.2);
    }
}