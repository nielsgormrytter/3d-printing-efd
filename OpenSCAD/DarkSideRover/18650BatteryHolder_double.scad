$fn = 100;
batteryDiameter = 18;
holderHeight = 15;
doubleHeight = 65+5;
holeSize= 1.5;
nBatteries=2;
holderOffset = 0.8;

module holder(){
    difference() {
        union(){
            cylinder(h=2, d=3);
            difference() {
                cylinder(h=holderHeight, d=batteryDiameter+2.5);
                translate([0, 0, 0.9]) cylinder(h=holderHeight, d=batteryDiameter+0.5);
                translate([-batteryDiameter/2-4/2,-batteryDiameter/2-2.5,1.5]) cube([batteryDiameter+4+5,7,holderHeight]);
                translate([-batteryDiameter/2-4/2,-batteryDiameter/2-8,0.9]) cube([batteryDiameter+4+5,batteryDiameter+5.7,1]);
            }
        }
        cylinder(h=holderHeight, d=holeSize);
    }
}

module plate(){
    difference(){
        translate([-(batteryDiameter+3)/2,batteryDiameter/2+0.7,0])cube([batteryDiameter+3.5,1.75,doubleHeight]);
        for(j = [0:10:30]){
            for(i = [0:10:60]){
                translate([j-5,13,i+5])rotate([90,0,0])cylinder(d=4.0,h=5);
            }
        }
    }
}

for(i = [0:20:(nBatteries-1)*20]) {
    difference() {
        union() {
            translate([i,0,0]) plate();
            translate([i,0,holderOffset]) holder();
            translate([i,0,doubleHeight-holderOffset])mirror([0,0,1]) holder();
        }
        for(k = [0:20:(nBatteries-2)*20]) {
            for(j = [0:doubleHeight-1.5:doubleHeight]) {
                translate([k+batteryDiameter/2+0.5,-batteryDiameter/2,j]) cube([1,batteryDiameter,2]);
            }
        }
    }
}