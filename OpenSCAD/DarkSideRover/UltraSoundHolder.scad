difference() {
    cube([45, 10, 4]);
    translate([0, 5, 1.0]) cube([45, 10, 2.0]);
    translate([3, 6, -0.5]) cube([39, 6, 5]);
    translate([13, 5, -0.5]) cube([19, 6, 5]);
    translate([1.0,6.0, -3]) rotate([0, 0, 90]) cylinder(10,d=1, $fn=100);
    translate([45-1.0,6.0, -3]) rotate([0, 0, 90]) cylinder(10,d=1, $fn=100);
    for(i = [2:2:43]) {
        translate([i, 5, 2]) rotate([90, 0, 0]) cylinder(10, d=1.0, $fn=100);
    }
}
