$fn=60;
thickness=1.75;
plateLength = 80;
arduinoHoleRadius = 1.6;
merkurHoleRadius = 2.0;
difference() {
    union(){cube([plateLength,18,thickness]);
    linear_extrude(height = 3){        
    translate([9,5,1])text("Joséphine");
     }}
     translate([5, 9, 0]) cylinder(h=thickness, r=merkurHoleRadius);
     translate([plateLength-5, 9, 0]) cylinder(h=thickness, r=merkurHoleRadius);
 }