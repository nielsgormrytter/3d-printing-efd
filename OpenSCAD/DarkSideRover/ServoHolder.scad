module cutout() {
difference() {
    rotate([45,0,0]) cube([33,9,9]);
    translate([-0.5,-6,11]) cube([34,10,10]);
    translate([-0.5,-16.3,0]) cube([34,10,10]);	}
}

module cageUp() {
    difference() {
        union() {
            translate([-12.5,-8,0]) cube([25,16,17.2]);
            difference() {
                translate([-16.1,-8,10]) cube([32.2,16,7.2]);
                translate([11,-8.25,11]) rotate([0,45,0]) cube([30, 16.5, 30]);		
                translate([-11,-8.25,11]) rotate([0,-135,0]) cube([10, 16.5, 10]);
            }
        }
        translate([-12.30,-6.3,1]) cube([24.5,12.6,16.3]);
        translate([-13,-6.3,3]) cube([26,12.6,7]);
        translate([-16.5,0,3.65]) cutout();
        translate([-10,-8.1,1]) cube([20,16.2,10]);
        translate([-5,-8.1,1]) cube([10,16.2,14.7]);
        translate([-3.7,-10,4.65]) rotate([0, 0, 90]) cutout();
        translate([3.7,-10,4.65]) rotate([0, 0, 90]) cutout();
        translate([13.75,0,14.3]) cylinder(r=1,h=3,$fn=45);
        translate([-13.75,0,14.3]) cylinder(r=1,h=3,$fn=45);
    }
}


translate([-5.5,0,2]) cageUp();
difference() {
    translate([-18,-8,-3]) cube([25,16,6]);
    translate([0,0, 10]) cylinder(h=100, r=2, center = true, $fn=45);
    translate([-10, 0, -10]) cylinder(h=100, r=2, center = true, $fn=45);
}
