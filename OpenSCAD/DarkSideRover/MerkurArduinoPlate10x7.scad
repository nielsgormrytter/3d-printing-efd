//Arduino hole spacing from
//https://www.flickr.com/photos/johngineer/5484250200/sizes/o/in/photostream/
$fn=60;
thickness=1.75;
arduinoHoleRadius = 1.5;
merkurHoleRadius = 2.0;
difference() {
    translate([0,0,0]) cube([100,70,thickness]);
    union() {
        translate([8+1.3, 10+2.5, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        translate([8+1.3, 10+53.3 - 2.5, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        translate([8+1.3+50.8, 10+7.3, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        translate([8+1.3+50.8, 10+7.3+27.9, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        translate([70, 20, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        translate([70+26, 20, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
        for (i=[5:10:95]) {
            for (j=[5:10:65]) {
                translate([i, j, 0]) cylinder(h=thickness, r=merkurHoleRadius);
            }
        }
    }
}