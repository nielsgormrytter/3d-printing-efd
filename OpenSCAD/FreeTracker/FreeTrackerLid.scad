$fn=15;
wallThickness = 1.85;
interiorLength = 80;
interiorWidth = 40;
interiorHeight = 30;
difference() {
    union() {
        translate([-wallThickness, -wallThickness, -wallThickness]) cube([interiorLength+2*wallThickness, interiorWidth+2*wallThickness, wallThickness]);
        translate([0.1,0,0]) cube([12, 3, 12]);
        translate([0.1,interiorWidth-3,0]) cube([12, 3, 12]);
        translate([interiorLength-12-0.1,interiorWidth-3,0]) cube([12, 3, 12]);
        translate([interiorLength-12-0.1,0,0]) cube([12, 3, 12]);
    }
    translate([5, -wallThickness, 5]) rotate([-90, 0, 0]) cylinder(interiorWidth+2*wallThickness, d=3.2);
    translate([interiorLength-5, -wallThickness, 5]) rotate([-90, 0, 0]) cylinder(interiorWidth+2*wallThickness, d=3.2);
}