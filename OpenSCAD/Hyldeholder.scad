linear_extrude(height = 1.6, center = true, convexity = 10, twist = 0){
polygon(points=[[0.9,1],[11,1],[0.9,6]]);}
translate(v = [0, 4.5, 0])
cube(size=[2,9,11],center=true);
translate(v = [6, 1.5/2, 0])
cube(size=[12,1.5,11],center=true);
rotate(a=[90,0,0])
translate(v = [1.15, 0, -0.1])
cylinder(h = 8, r=2.4,$fn=100);