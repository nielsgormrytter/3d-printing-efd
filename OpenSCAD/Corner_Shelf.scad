$fn=64;
fraction=4; 
X=150;
Y=100;
Z=50;
Wallthickness=3;
translate([0,0,0]) sector(Z, X*2, (360/4));
module sector(h, d, a2) {
difference(){
union(){
    difference() {
            cylinder(h=h, d=d);
            rotate([0,0,0]) translate([-d/2, -d/2, -0.5])
                cube([d, d/2, h+1]);
            #rotate([0,0,a2]) translate([-d/2, -(X-Y)/2, -0.5])
                cube([d, d/2+(X-Y)/2, h+1]);
}
}
union(){
    translate([Wallthickness,Wallthickness,5])difference() {
            cylinder(h=Z, d=d-Wallthickness*4);
            rotate([0,0,0]) translate([-(d-Wallthickness*2)/2, -(d-Wallthickness*2)/2, -0.5])
                cube([(d-Wallthickness*2), (d-Wallthickness*2)/2, h+1]);
            rotate([0,0,a2]) translate([-(d-Wallthickness*2)/2, -(X-Y)/2, -0.5])
                cube([(d-Wallthickness*2), (d-Wallthickness*2)/2, h+1]);
}
}
}
}