thickness = 2;
scale([0.4, 0.4, 1/2])
    difference() {
        cube([150,150,thickness]);
        translate([0,0,thickness])
        mirror([0,0,1]) scale([1,1,thickness/140]) surface(file = "arduino-finalist-150x150.png");
}
