include <Container_Values.scad>;
$fn=100;
LidwallThickness=1;
LidThikness=1;
Tolerance=0.25;

Lidhole=0; // % of hole in lid

LidText=0;  //Do you want text on the lid?
Text="Kaz";    //Input for your text
TextSize=8;    //Size of your text

LidWallThikness=LidwallThickness+Tolerance;

difference(){
    cylinder(d=DiaMeter+WallThikness*2+LidWallThikness*2+0.5,h=LidHeighT+LidwallThickness);
    translate([0,0,LidThikness])cylinder(d=DiaMeter+WallThikness*2+Tolerance,h=LidHeighT+LidwallThickness);
    for (i=[1:LockNumber]){  
rotate([0,0,(360/LockNumber)*i])translate([(DiaMeter+WallThikness*2)/2-0.2,0,LidHeighT/2+LidwallThickness])sphere(d=LockSize);  
    if(LidText==1){
        mirror([1,0,0])translate([0,-TextSize/2,-1])linear_extrude(height = 1.5) {
text(str(Text),font="Liberation Sans:style=Bold", size = TextSize,halign="center");
    }
}
}
cylinder(d=DiaMeter*Lidhole/100,h=LidHeighT);
}