include <Container_Values.scad>;
$fn=64;
CubeDiameter=(CubeLength+CubeWidth)/(70/CubeSoftness);
CubeLidthickness=1;
LidwallThickness=1;
Tolerance=0.1;

Lidhole=0; // % of hole in lid

LidText=1;  //Do you want text on the lid?
Text="Flying Hacker Lab";    //Input for your text
TextSize=10;    //Size of your text


CubeLidwallthickness=LidwallThickness+Tolerance;

difference(){
hull(){
    translate([-CubeLidwallthickness-CubeWallthickness+(CubeDiameter/2),-CubeLidwallthickness-CubeWallthickness+(CubeDiameter/2),0])cylinder(d=CubeDiameter,h=CubeLidheight+CubeLidthickness);
    translate([CubeLength-(CubeDiameter/2)+CubeLidwallthickness+CubeWallthickness,-CubeLidwallthickness-CubeWallthickness+(CubeDiameter/2),0])cylinder(d=CubeDiameter,h=CubeLidheight+CubeLidthickness);
    translate([CubeLength+CubeLidwallthickness+CubeWallthickness-(CubeDiameter/2),CubeWidth+CubeLidwallthickness+CubeWallthickness-(CubeDiameter/2),0])cylinder(d=CubeDiameter,h=CubeLidheight+CubeLidthickness);
    translate([-CubeLidwallthickness-CubeWallthickness+(CubeDiameter/2),CubeWidth+CubeLidwallthickness+CubeWallthickness-(CubeDiameter/2),0])cylinder(d=CubeDiameter,h=CubeLidheight+CubeLidthickness);
}
translate([0,0,CubeLidthickness])hull(){
    translate([(CubeDiameter/2)-Tolerance-CubeWallthickness,(CubeDiameter/2)-Tolerance-CubeWallthickness,0])cylinder(d=CubeDiameter,h=CubeLidheight+CubeLidthickness);
    translate([CubeLength-(CubeDiameter/2)+CubeWallthickness+Tolerance,(CubeDiameter/2)-Tolerance-CubeWallthickness,0])cylinder(d=CubeDiameter,h=CubeLidheight+CubeLidthickness);
    translate([CubeLength-(CubeDiameter/2)+Tolerance+CubeWallthickness,CubeWidth-(CubeDiameter/2)+Tolerance+CubeWallthickness,0])cylinder(d=CubeDiameter,h=CubeLidheight+CubeLidthickness);
    translate([(CubeDiameter/2)-Tolerance-CubeWallthickness,CubeWidth-(CubeDiameter/2)+Tolerance+CubeWallthickness,0])cylinder(d=CubeDiameter,h=CubeLidheight+CubeLidthickness);
}
translate([0,0,CubeLidheight/2+CubeLidthickness])union(){
translate([CubeLength/2,-CubeWallthickness+0.2,0])sphere(d=LockSize);
translate([CubeLength/2,CubeWidth+CubeWallthickness-0.2,0])sphere(d=LockSize);
translate([-CubeWallthickness+0.2,CubeWidth/2,0])sphere(d=LockSize);
translate([CubeLength+CubeWallthickness-0.2,CubeWidth/2,0])sphere(d=LockSize);

}
if(LidText==1){
        mirror([1,0,0])translate([-CubeLength/2,CubeWidth/2-TextSize/2,-1])linear_extrude(height = 1.5) {
text(str(Text),font="Liberation Sans:style=Bold", size = TextSize,halign="center");
    }
}
#translate([(CubeLength-(CubeLength*Lidhole/100))/2,(CubeWidth-(CubeWidth*Lidhole/100))/2,0])cube([CubeLength*Lidhole/100,CubeWidth*Lidhole/100,CubeLidthickness]);
}