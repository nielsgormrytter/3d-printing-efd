$fn=100;
Pi=3.141592653589793238;
TokenNumber=12;
module Hole(){
      translate([-0.5*(TokenNumber*30)/Pi,0,2])cylinder(d=26,h=8+3*5);
    translate([-0.5*(TokenNumber*30)/Pi-16.5,-8,2])cube([34,16,8+3*5]); 
}
difference(){
    cylinder(d=(TokenNumber*30)/Pi+30,h=10+3*5);
    translate([0,0,2])cylinder(d=(TokenNumber*30)/Pi-30,h=8+3*5);
  for (i=[0:TokenNumber]){
    #rotate([0,0,360/TokenNumber*i])Hole();
  }  
}
 /*
for (i=[1:TokenNumber]){  
rotate([0,0,(360/TokenNumber)*i+(360/(TokenNumber*2))])translate([-0.5*(TokenNumber*30)/Pi-15,0,1])sphere(d=2);  
 }
*/