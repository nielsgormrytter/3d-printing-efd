$fn=64;
screwhole=3;
linehole=3.5;
difference(){
hull(){
translate([5,10,0])cylinder(d=20,h=3);
translate([110,10,0])cylinder(d=20,h=3);
translate([5,72.7,0])cylinder(d=20,h=3);
translate([110,72.7,0])cylinder(d=20,h=3);
}
translate([11.5,7,1])cylinder(d=screwhole,h=5);
translate([40,7,1])cylinder(d=screwhole,h=5);
translate([6.5,58.5,1])cylinder(d=screwhole,h=5);
translate([54.5,57.5,1])cylinder(d=screwhole,h=5);
for(j = [0:10:110]){ 
for(i = [0:10:70]){
    if(j==40&&i==0) {}
    else if (j==10&&i==0){}
    else if (j==50&&i==50){}
    else{
        translate([2+j,6+i,-1])cylinder(d=linehole,h=5);
    }
}
}
}
difference(){
translate([11.5,7,2])cylinder(d=5,h=5.5);
translate([11.5,7,1])cylinder(d=screwhole,h=7);
}
difference(){
translate([40,7,2])cylinder(d=5,h=5.5);
translate([40,7,1])cylinder(d=screwhole,h=7);
}
difference(){
translate([6.5,58.5,2])cylinder(d=5,h=5.5);
translate([6.5,58.5,1])cylinder(d=screwhole,h=7);
}
difference(){
translate([54.5,57.5,2])cylinder(d=5,h=5.5);
translate([54.5,57.5,1])cylinder(d=screwhole,h=7);
}
difference(){
translate([-5,10,0])cube([3,62.7,23]);
    for(j = [0:10:10]){   
    for(i = [0:10:60]){   
translate([-6,6+i,8+j])rotate([0,90,0])cylinder(d=linehole,h=6);
    }
}
}
difference(){
translate([117,10,0])cube([3,62.7,23]);
    for(j = [0:10:10]){   
    for(i = [0:10:60]){   
translate([116,6+i,8+j])rotate([0,90,0])cylinder(d=linehole,h=6);
    }
}
}