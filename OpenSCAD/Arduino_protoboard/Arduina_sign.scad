include <ConnectorPin.scad>;
$fn=60;
plateWidth = 32;
plateHeight = 14;
connectorOffset = 6;
holeSpacing = 17.3;
holeOffset = (plateWidth - holeSpacing) / 2; 
module Sign(){
    difference(){
    cube([49,13,2]);
linear_extrude(height = 3){        
translate([1,2,1])scale([1,1,2])text("Arduina");
     }
}
}
difference() {
    union() {
        cube([plateWidth, plateHeight, 2]);
        connectorPin([connectorOffset +  0, 7, 1.9]);
        connectorPin([connectorOffset + 10, 7, 1.9]);
        connectorPin([connectorOffset + 20, 7, 1.9]);
    }
}
mirror(v=[1,0,0]) {
translate([-32,13,0])scale([0.653,0.653,1])Sign();
}