$fn=64;
screwhole=3;
linehole=3.5;
difference(){
hull(){
translate([5,2,0])cylinder(d=20,h=13);
translate([115,2,0])cylinder(d=20,h=13);
translate([5,78.7,0])cylinder(d=20,h=13);
translate([115,78.7,0])cylinder(d=20,h=13);
}
hull(){
translate([8,5,3])cylinder(d=20,h=13);
translate([112,5,3])cylinder(d=20,h=13);
translate([8,75.7,3])cylinder(d=20,h=13);
translate([112,75.7,3])cylinder(d=20,h=13);
}
translate([11.5,7+-3,1])cylinder(d=screwhole,h=5);
translate([40,7+-3,1])cylinder(d=screwhole,h=5);
translate([6.5,53.5+2,1])cylinder(d=screwhole,h=5);
translate([54.5,52.5+2,1])cylinder(d=screwhole,h=5);
for(j = [0:10:110]){ 
for(i = [0:10:80]){
    if(j==40&&i==0) {}
    else if (j==10&&i==0){}
//    else if (j==50&&i==50){}
    else{
        translate([5+j,0+i,-1])cylinder(d=linehole,h=5);
    }
}
}
    for(i = [10:10:70]){   
translate([-6,0+i,8])rotate([0,90,0])cylinder(d=linehole,h=7);
translate([121,0+i,8])rotate([0,90,0])cylinder(d=linehole,h=7);
    }   
for(i = [10:10:100]){   
    translate([5+i,-2,8])rotate([90,0,0])cylinder(d=linehole,h=7);
}
for(i = [20:10:100]){   
    if(i==30){}
    else if(i==40){}
    else{
    translate([5+i,90,8])rotate([90,0,0])cylinder(d=linehole,h=7);
    }
}
$fn=50;
    minkowski() {
    cube([6,10,6]);
translate([8,81,9])rotate([90,0,0])cylinder(r=2,h=2);
}
minkowski() {
    cube([9,10,6]);
translate([37,81,9])rotate([90,0,0])cylinder(r=2,h=2);
}    
}
difference(){
translate([11.5,7+-3,2])cylinder(d=5,h=5.5);
translate([11.5,7+-3,1])cylinder(d=screwhole,h=7);
}
difference(){
translate([40,7+-3,2])cylinder(d=5,h=5.5);
translate([40,7+-3,1])cylinder(d=screwhole,h=7);
}
difference(){
translate([6.5,53.5+2,2])cylinder(d=5,h=5.5);
translate([6.5,53.5+2,1])cylinder(d=screwhole,h=7);
}
difference(){
translate([54.5,52.5+2,2])cylinder(d=5,h=5.5);
translate([54.5,52.5+2,1])cylinder(d=screwhole,h=7);
}