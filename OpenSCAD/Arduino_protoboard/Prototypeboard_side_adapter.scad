rod="m3_rod.stl";
center=0;  // Center or by the side?
hole=1; // Holes or cylinders?
click=1; // Clickers or holes?
scaling=1.2;
$fn=64;
distance_standart=10;
distance_adapter=18;
distance_standart_adapter=8; //only for "center=0"
module clicker (){
difference(){
    translate([0,0,2])cylinder(d=3,h=1.5);
    translate([0,0,2.5])cube([1,5,1],center=true);
}
difference(){
    translate([0,0,-2])cylinder(d1=3.4,d2=5,h=5);
    translate([0,0,1])cube([1,5,6],center=true);
    difference(){
        translate([0,0,-1.2])cylinder(d=5,h=3.5);
        translate([0,0,-2])cylinder(d=3,h=5);
    }
}
}
if (center==0){
difference(){
    cube([10+distance_standart+distance_standart_adapter+distance_adapter,10,3]);
    if(click==0){
    translate([5,5,-1])cylinder(d=3.5,h=7);    
    translate([5+distance_standart,5,-1])cylinder(d=3.5,h=7);    
    }
    if (hole==0){
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5+distance_standart+distance_standart_adapter,5,1])scale(scaling,scaling,0)import(rod);
    }
    if (hole==1){
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,-1])cylinder(d=3.5,h=7);    
    translate([5+distance_standart+distance_standart_adapter,5,-1])cylinder(d=3.5,h=7);    
    }
}
if (click==1){
    translate([5,5,-3])clicker();
    translate([5+distance_standart,5,-3])clicker();
}
if (hole==0){
difference(){
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,2])cylinder(d=5,h=5.5);
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,1])cylinder(d1=3,d2=4,h=2);    
}
difference(){
    translate([5+distance_standart+distance_standart_adapter,5,2])cylinder(d=5,h=5.5);
    translate([5+distance_standart+distance_standart_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5+distance_standart+distance_standart_adapter,5,1])cylinder(d1=3,d2=4,h=2);    
}
}
}
if (center==1){
difference(){
    cube([10+distance_adapter,10,3]);
    if (click==0){
    translate([5+(distance_adapter-distance_standart)/2,5,-1])cylinder(d=3.5,h=7);    
    translate([5+(distance_adapter-distance_standart)/2+distance_standart,5,-1])cylinder(d=3.5,h=7);    
    }
    if(hole==0){
    translate([5+distance_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5,5,1])scale(scaling,scaling,0)import(rod);
    }
    if(hole==1){
         translate([5+distance_adapter,5,-1])cylinder(d=3.5,h=7);
        translate([5,5,-1])cylinder(d=3.5,h=7);
}
}
if(hole==0){
difference(){
    translate([5+distance_adapter,5,2])cylinder(d=5,h=5.5);
    translate([5+distance_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5+distance_adapter,5,1])cylinder(d1=3,d2=4,h=2);    
}
difference(){
    translate([5,5,2])cylinder(d=5,h=5.5);
    translate([5,5,1])scale(scaling,scaling,0)import(rod);
    translate([5,5,1])cylinder(d1=3,d2=4,h=2);    
}
}
if (click==1){
    translate([5+(distance_adapter-distance_standart)/2,5,-3])clicker();
    translate([5+(distance_adapter-distance_standart)/2+distance_standart,5,-3])clicker();
}
}