fraction=12;
// 2=Black, 3=Grey, 4=White, 5=Green 6=Blue, 7=Transparent, 8=Red, 10=orange, 12=Purple.
$fn=64;
difference(){
    cylinder(d=102,h=5);
    translate([0,0,1])cylinder(d=100,h=20);
}
for(i = [0:1:fraction]){
rotate([0,0,(360/fraction)*i])translate([0,-0.5,0])cube([50,1,5]);
}