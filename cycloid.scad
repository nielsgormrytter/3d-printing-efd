for(t=[-8 : 0.01 : +8])
    translate([8 * (t - sin(t*180/3.14)), 8 * (1-cos(t*180/3.14)), 0])
        cylinder(r=1.5, h=6, center=true);
